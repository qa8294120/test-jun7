import LoginPage from '../support/pageobjects/loginPage';
import SearchPage from '../support/pageobjects/searchpage';

describe('Search Functionality', () => {
  const loginPage = new LoginPage();
  const searchpage = new SearchPage();

  beforeEach(() => {
    cy.fixture('search').then((data) => {
      loginPage.visitLoginPage();
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
    });
  });

  it('should search for package', () => {
    cy.fixture('search').then((data) => {
      searchpage.performSearch(data.searchTerms.package);
      searchpage.selectSearchResult();
      cy.get('body').should('be.visible');
    });
  });

  it('should search for Reading section', () => {
    cy.fixture('search').then((data) => {
      searchpage.performSearch(data.searchTerms.readingSection);
      searchpage.selectSearchResult();
    });
  });

  it('should search for Ielts package', () => {
    cy.fixture('search').then((data) => {
      searchpage.performSearch(data.searchTerms.ieltsPackage);
      searchpage.selectSearchResult();
    });
  });

  it('should search for important questions', () => {
    cy.fixture('search').then((data) => {
      searchpage.performSearch(data.searchTerms.importantQuestions);
      searchpage.selectSearchResult();
      cy.get('body').should('be.visible');
    });
  });
});
