import LoginPage from '../support/pageobjects/signin';

describe('Login Test', () => {
  const loginPage = new LoginPage();

  beforeEach(() => {
    loginPage.visitLoginPage();
  });

  it('should login with OTP', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      loginPage.verifySuccessfulLogin();
    });
  });

  it('should show error for unregistered number', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.unregisteredNumber);
      loginPage.clickLoginButton();
      loginPage.verifyErrorNotification();
    });
  });

  it('should show error for null mobile number', () => {
    loginPage.clickLoginButton();
    loginPage.verifyFieldErrorMessage();
  });

  it('should show error for using special characters', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.specialCharactersNumber);
      loginPage.clickLoginButton();
      loginPage.verifyFieldErrorMessage();
    });
  });

  it('should show error for invalid number', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.invalidMobileNumber);
      loginPage.clickLoginButton();
      loginPage.verifyInvalidNumberError();
    });
  });

  it('should show error for invalid landline number', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.invalidLandlineNumber);
      loginPage.clickLoginButton();
      loginPage.verifyInvalidLandlineError();
    });
  });
});
