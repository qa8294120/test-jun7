import LoginPage from '../support/pageobjects/loginPage';
import VideoPage from '../support/pageobjects/videoPage';

describe('OTP Authentication', () => {
  const loginPage = new LoginPage();
  const videoPage = new VideoPage();

  beforeEach(() => {
    loginPage.visitLoginPage();
  });

  it('should successfully redirect to videos', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.verifyVideoPage();
    });
  });

  it('should successfully play videos', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.verifyVideoPage();
      videoPage.clickOnVideo();
    });
  });

  it('should successfully make video interactive', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.clickOnVideo();
      videoPage.toggleVideo();
    });
  });

  it('should successfully redirect to ask section', () => {
    cy.fixture('login').then((data) => {
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
      videoPage.clickOnCourse();
      videoPage.clickOnVideo();
      videoPage.goToAskSection();
    });
  });
});
