

describe('OTP Authentication', () => {
    it('should login with OTP', () => {
        // Visit the login page
    
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type (9811313200)
        cy.get('.q-btn__content').click()
        cy.get('.d-flex > .q-item__section--main > .package-nav-overflow').should('be.visible')

        //cy.get('.q-form > :nth-child(2)').type(729155)
        //cy.get('.q-btn__content').click()
        //successful login
    })

    it('should show error for unregistered number', () => {
        // Visit the login page
    
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type (9811313208)
        cy.get('.q-btn__content').click()
        cy.get('.q-notification__message').should('be.visible')
        //cy.get('.q-form > :nth-child(2)').type(729155)
        //cy.get('.q-btn__content').click()
        //shows error

})


it('should show error for null mobile number', () => {
    
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type 
    cy.get('.q-btn__content').click()
    cy.get('.q-form > :nth-child(2)').type(729155)
    cy.get('.q-btn__content').click()
    cy.get('.q-field__messages > div').should('be.visible')
    //shows error



})

it('should show error for using special characters', () => {
    
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type ('@sg')
    cy.get('.q-btn__content').click()
    cy.get('.q-form > :nth-child(2)').type(729155)
    cy.get('.q-btn__content').click()
    cy.get('.q-field__messages > div').should('be.visible')

    //shows error



})



it('should show error for invalid number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type ('98113132')
    cy.get('.q-btn__content').click()
    cy.get('.q-form > :nth-child(2)').type(729155)
    cy.get('.q-btn__content').click()
    cy.get('.q-notification__message').should('be.visible')



})

it.only('should show error for invalid landline number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type ('021543543')
    cy.get('.q-btn__content').click()
    cy.get('.q-form > :nth-child(2)').type(729155)
    cy.get('.q-btn__content').click()
    cy.get('.tw-text-sm').should('be.visible')
    



})


})