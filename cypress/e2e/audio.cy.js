import LoginPage from '../support/pageobjects/loginPage';
import AudioPage from '../support/pageobjects/audioPage';

describe('Login to Ambition Guru with Mobile Number and OTP', () => {
  const loginPage = new LoginPage();
  const audioPage = new AudioPage();

  beforeEach(() => {
    cy.fixture('audio').then((data) => {
      loginPage.visitLoginPage();
      loginPage.fillMobileNumber(data.validMobileNumber);
      loginPage.clickLoginButton();
    });
  });

  it('Should login with mobile number and OTP and redirect to the audio section', () => {
    audioPage.clickAudioTab();
    cy.url().should('include', '/audio'); // Assuming the URL changes to include /audio
  });

  it('Should play the audio', () => {
    audioPage.clickAudioTab();
    audioPage.playAudio();
    cy.get('audio').should('have.prop', 'paused', false); // Assuming an audio element is present and playing
  });
});
