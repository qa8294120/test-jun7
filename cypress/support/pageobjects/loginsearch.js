class LoginSearch {
    visitLoginPage() {
      cy.visit('https://app.ambition.guru/login');
    }
  
    fillMobileNumber(mobileNumber) {
      cy.get('.q-field__control').type(mobileNumber);
    }
  
    clickLoginButton() {
      cy.get('.q-btn__content').click();
    }
  }
  
  export default LoginSearch;
  