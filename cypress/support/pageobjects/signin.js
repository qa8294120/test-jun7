class LoginPage {
    visitLoginPage() {
      cy.visit('https://app.ambition.guru/login');
    }
  
    fillMobileNumber(number) {
      cy.get('.q-form > :nth-child(1)').type(number);
    }
  
    fillOTP(otp) {
      cy.get('.q-form > :nth-child(2)').type(otp);
    }
  
    clickLoginButton() {
      cy.get('.q-btn__content').click();
    }
  
    verifySuccessfulLogin() {
      cy.get('.d-flex > .q-item__section--main > .package-nav-overflow').should('be.visible');
    }
  
    verifyErrorNotification() {
      cy.get('.q-notification__message').should('be.visible');
    }
  
    verifyFieldErrorMessage() {
      cy.get('.q-field__messages > div').should('be.visible');
    }
  
    verifyInvalidNumberError() {
      cy.get('.q-notification__message').should('be.visible');
    }
  
    verifyInvalidLandlineError() {
      cy.get('.tw-text-sm').should('be.visible');
    }
  }
  
  export default LoginPage;
  