class LoginPage {
    visitLoginPage() {
      cy.visit('https://app.ambition.guru/login');
    }
  
    fillMobileNumber(mobileNumber) {
      cy.get('.q-form > :nth-child(1)').type(mobileNumber);
    }
  
    clickLoginButton() {
      cy.get('.q-btn__content').click();
    }
  }
  
  export default LoginPage;
  