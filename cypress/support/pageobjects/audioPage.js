class AudioPage {
    clickAudioTab() {
      cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click();
    }
  
    playAudio() {
      cy.get('.carousel__slide--active > .p-10 > .tw-gap-6 > .relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click();
    }
  }
  
  export default AudioPage;
  